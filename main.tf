module "cluster_k8s" {
  source      = "./modules/k8s"
  network_id  = "${module.cluster_network.network_id}"
  subnet_id  = "${module.cluster_network.subnet_id}"
  zone  = "${module.cluster_network.zone}"
  security_group_ids  = "${module.cluster_network.security_group_ids}"

}

# module "cluster_postgres" {
#   source      = "./modules/postgresql"
#     network_id  = "${module.cluster_network.network_id}"
#   subnet_id  = "${module.cluster_network.subnet_id}"
#   zone  = "${module.cluster_network.zone}"
#   security_group_ids  = "${module.cluster_network.security_group_ids}"

# }

# module "s3_bucket" {
#   source      = "./modules/s3"

# }

module "cluster_instance" {
  source      = "./modules/instances"
    network_id  = "${module.cluster_network.network_id}"
  subnet_id  = "${module.cluster_network.subnet_id}"
  zone  = "${module.cluster_network.zone}"
  security_group_ids  = "${module.cluster_network.security_group_ids}"
 
}

module "cluster_network" {
  source      = "./modules/net"

}




