
data "yandex_compute_image" "ubuntu-20-04" {
  family = var.vpn_os_type
}

resource "yandex_compute_instance" "vpn_instance" {
  name        = "vpn"
  platform_id = var.vpn_platform_id
  zone        = var.yc_zone
  allow_stopping_for_update = true

  resources {
    cores  = 2
    memory = 2
    core_fraction = 20
  }

  boot_disk {
    initialize_params {
      type = var.vpn_disk_type
      size = var.vpn_disk_size
      image_id = data.yandex_compute_image.ubuntu-20-04.id
    }
  }
 
  scheduling_policy {
    preemptible = true
  }
 
  network_interface {
    nat = true
    subnet_id = var.subnet_id
  }

  metadata = {
    user-data = "${file("${path.module}/meta.txt")}"
  }
}
  





