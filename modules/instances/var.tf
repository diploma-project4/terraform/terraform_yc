
### provider settings

variable "yc_zone" {
  description = "k8s cluster version"
  type        = string
  default = "ru-central1-a"
  sensitive = false
}


### vpn instance


variable "vpn_disk_type" {
  description = "disk type"
  type        = string
  default = "network-hdd"
  sensitive = false
}

variable "vpn_platform_id" {
  description = "platform id"
  type        = string
  default = "standard-v3"
  sensitive = false
}

variable "vpn_os_type" {
  description = "OS type"
  type        = string
  default = "ubuntu-2004-lts"
  sensitive = false
}

variable "vpn_disk_size" {
  description = "disk size"
  type        = number
  default = 20
  sensitive = false
}

##
 
variable "security_group_ids" {
  description = "security_group_ids"
  type = list
  sensitive = true
}

variable "zone" {
  description = "zone"
  type = string
  sensitive = true
}

variable "network_id" {
  description = "network_id"
  type = string
  sensitive = true
}

variable "subnet_id" {
  description = "subnet_id"
  type = string
  sensitive = true
}
