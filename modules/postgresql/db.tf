


resource "yandex_mdb_postgresql_cluster" "diploma_psql" {
  name        = var.postgresql_cluster_name
  environment = var.postgresql_environment
  ##down
  security_group_ids  = var.security_group_ids
  #down
  network_id  = var.network_id
  deletion_protection = false

  config {
    version = var.postgresql_version
    resources {
      resource_preset_id = var.postgresql_resource_type
      disk_type_id       = var.postgresql_disk_type
      disk_size          = var.postgresql_disk_size
    }
  }

  maintenance_window {
    type = "WEEKLY"
    day  = "SAT"
    hour = 12
  }

  host {
    zone      = var.yc_zone
    ## down
    subnet_id = var.subnet_id
  }
}


resource "yandex_mdb_postgresql_database" "diploma_db" {
  cluster_id = yandex_mdb_postgresql_cluster.diploma_psql.id
  name       = var.postgresql_db_name
  owner      = yandex_mdb_postgresql_user.diploma_psql_user.name
  depends_on = [
    yandex_mdb_postgresql_user.diploma_psql_user
  ]
}

resource "yandex_mdb_postgresql_user" "diploma_psql_user" {
  cluster_id = yandex_mdb_postgresql_cluster.diploma_psql.id
  name       = var.postgresql_db_user_name
  password   = var.postgresql_db_user_pass
}


