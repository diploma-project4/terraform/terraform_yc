
### provider settings

variable "yc_zone" {
  description = "k8s cluster version"
  type        = string
  default = "ru-central1-a"
  sensitive = false
}

### postgresql var

variable "postgresql_cluster_name" {
  description = "postgresql cluster name"
  type        = string
  default = "diploma_cluster"
  sensitive = false
}


variable "postgresql_environment" {
  description = "environment"
  type        = string
  default = "PRESTABLE"
  sensitive = false
}

variable "postgresql_version" {
  description = "version"
  type        = number
  default = 15
  sensitive = false
}

variable "postgresql_disk_size" {
  description = "disk size"
  type        = number
  default = 10
  sensitive = false
}

variable "postgresql_disk_type" {
  description = "disk type"
  type        = string
  default = "network-hdd"
  sensitive = false
}

variable "postgresql_resource_type" {
  description = "cluster size"
  type        = string
  default = "b1.medium"
  sensitive = false
}


### postgresql db & user

variable "postgresql_db_name" {
  description = "name of db"
  type        = string
  default = "testdb"
  sensitive = false
}

variable "postgresql_db_user_name" {
  description = "name of user"
  type        = string
  default = "user1"
  sensitive = false
}

variable "postgresql_db_user_pass" {
  description = "pass for postgresql cluster"
  type        = string
  default = "rootroot"
  sensitive = true
}
### from net module
 
variable "security_group_ids" {
  description = "security_group_ids"
  type = list
  sensitive = true
}

variable "zone" {
  description = "zone"
  type = string
  sensitive = true
}

variable "network_id" {
  description = "network_id"
  type = string
  sensitive = true
}

variable "subnet_id" {
  description = "subnet_id"
  type = string
  sensitive = true
}

