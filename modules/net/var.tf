### provider settings

variable "yc_zone" {
  description = "k8s cluster version"
  type        = string
  default = "ru-central1-a"
  sensitive = false
}

### network settings

variable "sub_cidr_block" {
  description = "IPs for subs"
  type = list
  default = ["10.2.0.0/16"]
  sensitive = true
}


variable "ingress_proto" {
  description = "ingress proto"
  type = string
  default = "ANY"
  sensitive = true
}

variable "ingress_sub" {
  description = "ingress subs"
  type = list
  default = ["0.0.0.0/0"]
  sensitive = true
}


variable "ingress_from_port" {
  description = "ingress from port"
  type = number
  default = 0
  sensitive = true
}

variable "ingress_to_port" {
  description = "ingress to port"
  type = number
  default = 65535
  sensitive = true
}



variable "egress_proto" {
  description = "egress proto"
  type = string
  default = "ANY"
  sensitive = true
}

variable "egress_sub" {
  description = "egress subs"
  type = list
  default = ["0.0.0.0/0"]
  sensitive = true
}


variable "egress_from_port" {
  description = "egress from port"
  type = number
  default = 0
  sensitive = true
}

variable "egress_to_port" {
  description = "egress to port"
  type = number
  default = 65535
  sensitive = true
}


variable "folder_id" {
  description = "folder id"
  type        = string
  default = "b1giaun5e0nomk1n1nu4"
  sensitive = false
}

