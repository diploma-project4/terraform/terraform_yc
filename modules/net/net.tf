

resource "yandex_vpc_network" "diploma_net" {
  name = "diploma_net"
}

resource "yandex_vpc_subnet" "diploma_sub" {
  v4_cidr_blocks = var.sub_cidr_block
  zone           = var.yc_zone
  network_id     = "${yandex_vpc_network.diploma_net.id}"
  route_table_id = yandex_vpc_route_table.rt.id
}

resource "yandex_vpc_gateway" "diploma_nat_gw" {
  folder_id      = var.folder_id
  name = "test-gateway"
  shared_egress_gateway {}
}

resource "yandex_vpc_route_table" "rt" {
  folder_id      = var.folder_id
  name       = "test-route-table"
  network_id = "${yandex_vpc_network.diploma_net.id}"

  static_route {
    destination_prefix = "0.0.0.0/0"
    gateway_id         = yandex_vpc_gateway.diploma_nat_gw.id
  }
}

resource "yandex_vpc_security_group" "diploma_secgr" {
  name        = "secure group for k8s cluster"
  description = "diploma project"
  network_id  = "${yandex_vpc_network.diploma_net.id}"


  ingress {
    protocol       = var.ingress_proto
    description    = "rule1 description"
    v4_cidr_blocks = var.ingress_sub
    from_port      = var.ingress_from_port
    to_port        = var.ingress_to_port
  }

  egress {
    protocol       = var.egress_proto
    description    = "rule2 description"
    v4_cidr_blocks = var.egress_sub
    from_port      = var.egress_from_port
    to_port        = var.egress_to_port
  }
}

