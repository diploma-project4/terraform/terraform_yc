output "network_id" {
   value = "${yandex_vpc_network.diploma_net.id}"
}
output "subnet_id" {
   value = "${yandex_vpc_subnet.diploma_sub.id}"
}

output "zone" {
    value = "${yandex_vpc_subnet.diploma_sub.zone}"
}

output "security_group_ids" {
    value = ["${yandex_vpc_security_group.diploma_secgr.id}"]
}