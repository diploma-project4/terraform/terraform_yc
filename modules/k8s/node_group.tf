
resource "yandex_kubernetes_node_group" "diploma_nodes" {
  cluster_id  = "${yandex_kubernetes_cluster.diploma_cluster.id}"
  name        = var.k8s_cluster_name
  description = "diploma"
  version     = var.k8s_ver


  instance_template {
    platform_id = var.k8s_platform_id

    metadata = {
    user-data = "${file("${path.module}/meta.txt")}"
}

    network_interface {
      nat                = false
      subnet_ids         = [var.subnet_id]
      security_group_ids = var.security_group_ids
    }

    resources {
      memory = 2
      cores  = 2
      core_fraction = 20
    }

    boot_disk {
      type = var.k8s_disk_type
      size = var.k8s_disk_size
    }

    scheduling_policy {
      preemptible = true
    }

    container_runtime {
      type = "containerd"
    }
  }

  scale_policy {
   auto_scale {
      min     = 1
      max     = 3
      initial = 2
    }
  }

  allocation_policy {
    location {
      zone = var.yc_zone
    }
  }

  maintenance_policy {
    auto_upgrade = true
    auto_repair  = true

    maintenance_window {
      day        = "monday"
      start_time = "15:00"
      duration   = "3h"
    }

    maintenance_window {
      day        = "friday"
      start_time = "10:00"
      duration   = "4h30m"
    }
  }
}



