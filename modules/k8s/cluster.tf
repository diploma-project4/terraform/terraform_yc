

resource "yandex_kubernetes_cluster" "diploma_cluster" {
  name        = var.k8s_cluster_name
  description = "test_terraform_cluster"

  network_id = var.network_id

  master {
    version = var.k8s_ver
    zonal {
      zone      = var.zone
      subnet_id = var.subnet_id
    }

    public_ip = true

    security_group_ids = var.security_group_ids

    maintenance_policy {
      auto_upgrade = true

      maintenance_window {
        start_time = "15:00"
        duration   = "3h"
      }
    }

    master_logging {
      enabled = false
    }
  }

  service_account_id      = "${yandex_iam_service_account.k8s_sa.id}"
  node_service_account_id = "${yandex_iam_service_account.node_sa.id}"


  release_channel = var.release_channel
  #network_policy_provider = var.network_policy_provider

  kms_provider {
    key_id = "${yandex_kms_symmetric_key.key-a.id}"
  }
  depends_on = [
  yandex_resourcemanager_folder_iam_member.k8s_sa,
  yandex_resourcemanager_folder_iam_member.node_sa
]
}

resource "yandex_kms_symmetric_key" "key-a" {
  name              = "example-symetric-key"
  description       = "description for key"
  default_algorithm = "AES_128"
  rotation_period   = "8760h" // equal to 1 year
}


