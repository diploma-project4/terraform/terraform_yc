

resource "yandex_iam_service_account" "k8s_sa" {
  name        = "k8s"
  description = "service account to manage K8S"
}

resource "yandex_iam_service_account" "node_sa" {
  name        = "node"
  description = "service account to manage K8S node gropup"
}

resource "yandex_resourcemanager_folder_iam_member" "node_sa" {
  count       = length(var.sa_role_node)
  folder_id   = var.sa_folder_id
  role        = var.sa_role_node[count.index]
  member      = "serviceAccount:${yandex_iam_service_account.node_sa.id}"
}

resource "yandex_resourcemanager_folder_iam_member" "k8s_sa" {
  count       = length(var.sa_role_k8s)
  folder_id   = var.sa_folder_id
  role        = var.sa_role_k8s[count.index]
  member      = "serviceAccount:${yandex_iam_service_account.k8s_sa.id}"
}
