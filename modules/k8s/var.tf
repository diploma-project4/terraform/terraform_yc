
### provider settings

variable "yc_zone" {
  description = "k8s cluster version"
  type        = string
  default = "ru-central1-a"
  sensitive = false
}

### k8s cluster var

variable "k8s_ver" {
  description = "k8s cluster version"
  type        = string
  default = "1.25"
  sensitive = false
}

  variable "release_channel" {
  description = "k8s cluster release_channel"
  type        = string
  default = "RAPID"
  sensitive = false
}

  variable "network_policy_provider" {
  description = "k8s cluster network_policy_provider"
  type        = string
  default = "CALICO"
  sensitive = false
}

variable "k8s_cluster_name" {
  description = "k8s cluster name"
  type        = string
  default = "diploma"
  sensitive = false
}

variable "k8s_platform_id" {
  description = "cluster type"
  type        = string
  default = "standard-v3"
  sensitive = false
}

variable "k8s_disk_size" {
  description = "disk size"
  type        = number
  default = 64
  sensitive = false
}

variable "k8s_disk_type" {
  description = "disk type"
  type        = string
  default = "network-hdd"
  sensitive = false
}



### service account

variable "sa_folder_id" {
  description = "folder id for sa"
  type        = string
  default = "b1giaun5e0nomk1n1nu4"
  sensitive = false
}

variable "sa_role_k8s" {
  description = "role for sa k8s"
  type        = list(string)
  default = ["k8s.clusters.agent","logging.writer","load-balancer.admin","vpc.publicAdmin"]
  sensitive = false
}

variable "sa_role_node" {
  description = "role for sa node"
  type        = list(string)
  default = ["container-registry.images.puller","container-registry.images.pusher"]
  sensitive = false
}


 ### from net module
variable "security_group_ids" {
  description = "security_group_ids"
  type = list
  sensitive = true
}

variable "zone" {
  description = "zone"
  type = string
  sensitive = true
}

variable "network_id" {
  description = "network_id"
  type = string
  sensitive = true
}

variable "subnet_id" {
  description = "subnet_id"
  type = string
  sensitive = true
}
