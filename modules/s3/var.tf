variable "folder_id" {
  description = "folder id"
  type        = string
  default = "b1giaun5e0nomk1n1nu4"
  sensitive = false
}

variable "yc_zone" {
  description = "k8s cluster version"
  type        = string
  default = "ru-central1-a"
  sensitive = false
}


# variable "secret_key" {
#   description = "k8s cluster version"
#   type        = string
#   default = "${yandex_iam_service_account_static_access_key.s3-static-key.secret_key}"
#   sensitive = false
# }


# variable "access_key" {
#   description = "k8s cluster version"
#   type        = string
#   default = "${yandex_iam_service_account_static_access_key.s3-static-key.access_key}"
#   sensitive = false
# }

output "access_key" {
    value = "${yandex_iam_service_account_static_access_key.s3-static-key.access_key}"
}

output "secret_key" {
    value = "${yandex_iam_service_account_static_access_key.s3-static-key.secret_key}"
}
