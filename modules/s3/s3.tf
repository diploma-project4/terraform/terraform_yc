
// Create SA
resource "yandex_iam_service_account" "s3" {
  folder_id = var.folder_id
  name      = "tf-test-sa"
}

// Grant permissions
resource "yandex_resourcemanager_folder_iam_member" "s3-editor" {
  folder_id = var.folder_id
  role      = "storage.editor"
  member    = "serviceAccount:${yandex_iam_service_account.s3.id}"
}

// Create Static Access Keys
resource "yandex_iam_service_account_static_access_key" "s3-static-key" {
  service_account_id = yandex_iam_service_account.s3.id
  description        = "static access key for object storage"
}

// Use keys to create bucket
resource "yandex_storage_bucket" "terr-backend" {
  access_key = yandex_iam_service_account_static_access_key.s3-static-key.access_key
  secret_key = yandex_iam_service_account_static_access_key.s3-static-key.secret_key
  bucket = "terraform-backend-bucket"
}




